Feature: Book flight ticket

Scenario Outline: As a user I want to book flight ticket with different locations
Given User first opens the browsers
Given Navigate to the URL Site
When user fills the from and to details with "<from>" and "<to>"
When user click on search flight button
Then It shows the different flight details
Then closes browser

Examples:
|from 			|to				|
|pune			|jaipur			|
|patna			|hyderabad		|