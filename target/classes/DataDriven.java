package utility;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class DataDriven {

	public static Properties prop;
	public static ArrayList<String> getData(String args) throws IOException 
	{
		// TODO Auto-generated method stub
		ArrayList<String> a=new ArrayList<String>();
		prop=reusableObjects.GenerateProp.generatepropobject();
		FileInputStream fis=new FileInputStream(prop.getProperty("excel_sheet_loc"));
		XSSFWorkbook workbook=new XSSFWorkbook(fis);
		int sheets=workbook.getNumberOfSheets();
		for(int i=0;i<sheets;i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("page1"))
			{
				XSSFSheet sheet=workbook.getSheetAt(i);
				Iterator<Row> rows=sheet.iterator();
				
				Row firstrow=rows.next();
				
				Iterator<Cell> ce=firstrow.cellIterator();
				
				int k=0;
				int column=0;
				while(ce.hasNext())
				{
					Cell value=ce.next();
					if(value.getStringCellValue().equalsIgnoreCase(args))
					{
						column=k;
					}
					k++;
				}
				//System.out.println(column);
				
				while(rows.hasNext())
				{
					Row r=rows.next();
					a.add(r.getCell(column).getStringCellValue());
					
				}
			}
		}
		return a;
	}

}
