package uiStore;

import org.openqa.selenium.By;

public class HomePageUI
{
	public static By from=By.name("flight_origin");
	public static By to=By.name("flight_destination");
	public static By searchFlightButton=By.id("BE_flight_flsearch_btn");
	public static By offerSection=By.cssSelector(".simple-tab.eventTrackable.dropdown-toggle.skipForLink.list-dropdownNull");
	public static By travelGuruIcon=By.xpath("(//a[@target='_blank'])[32]");
	public static By date=By.id("22/09/2021");
	public static By departureDate=By.id("BE_flight_origin_date");
}
