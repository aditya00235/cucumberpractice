package runner;

import org.apache.log4j.Logger;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import stepDefinitions.OfferValidation;

//@RunWith(Cucumber.class)
@CucumberOptions(features= {"src\\test\\resources\\featuresFiles"},
				glue= {"stepDefinitions"}
				)

/*public class TestRunner {
	
}*/

public class TestRunner extends AbstractTestNGCucumberTests	{
	
	
}