package pageObjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import reusableObjects.ReusableMethods;
import uiStore.OfferPageUI;

public class OfferPage 
{
	public static boolean validateDomesticFlightTab(WebDriver driver,Logger log)
	{
		if(ReusableMethods.getElements(OfferPageUI.domesticFlightTab, driver))
		{
			log.info("Domestic flight section tab found");
			return true;
		}
		return false;
	}

}
