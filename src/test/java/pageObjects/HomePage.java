package pageObjects;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import reusableObjects.GenerateProp;
import reusableObjects.ReusableMethods;
import uiStore.HomePageUI;
import uiStore.OfferPageUI;

public class HomePage 
{
	static Properties prop;
	public static boolean clickOffer(WebDriver driver,Logger log) throws Exception
	{
		if(ReusableMethods.getElements(HomePageUI.offerSection, driver))
		{
			log.info("Offer section identified");
			if(ReusableMethods.click(HomePageUI.offerSection, driver))
			{
				log.info("Clicked on offer section");
				return true;
				
			}
		}
		return false;
	}
	
	/*public static boolean travelGuruIconNavigation(WebDriver driver,Logger log) throws Exception
	{
		if(ReusableMethods.getElements(HomePageUI.travelGuruIcon, driver))
		{
			log.info(" Travel Guru Icon Identified");
			if(ReusableMethods.click(HomePageUI.travelGuruIcon, driver))
			{
				log.info("Travel Guru Icon clicked successfully");
				return true;
			}
			log.error("Not clicked");
		}
		log.error("Not identified");
		return false;
	}*/
	
	/*public static boolean bookFlights(WebDriver driver,Logger log) throws InterruptedException, Exception
	{
		prop=GenerateProp.generatepropobject();
		if(ReusableMethods.click(HomePageUI.from, driver))
			{
				WebElement from=driver.findElement(HomePageUI.from);
				//from.click();
				from.sendKeys(prop.getProperty("from"));
				Thread.sleep(2000L);
				log.info("benagluru keys send");
				//from.sendKeys(Keys.ARROW_DOWN);
				from.sendKeys(Keys.ENTER);
				
				WebElement to=driver.findElement(HomePageUI.to);
				to.sendKeys(prop.getProperty("to"));
				Thread.sleep(2000L);
				log.info("mumbai keys send");
				//to.sendKeys(Keys.ARROW_DOWN);
				to.sendKeys(Keys.ENTER);
			
				driver.findElement(HomePageUI.searchFlightButton).click();
				log.info("Button clicked successfully");
				return true;
			}
		
			return false;
	}*/



}
