

package stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.junit.Cucumber;
import pageObjects.HomePage;
import pageObjects.OfferPage;
import reusableObjects.Base;
import reusableObjects.GenerateProp;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

@RunWith(Cucumber.class)
public class OfferValidation {
	static WebDriver driver;
	Logger log=Logger.getLogger(OfferValidation.class);
	Properties prop;
	boolean flag;

	@Given("^User opens the browsers$")
	public void user_opens_the_browsers() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver=Base.initializeDriver();
	}

	@Given("^Navigate to URL Site$")
	public void navigate_to_URL_Site() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Properties prop=GenerateProp.generatepropobject();
        driver.get(prop.getProperty("url"));
        driver.manage().window().maximize();
	}

	@When("^User click on the offer section in the home page$")
	public void user_click_on_the_offer_section_in_the_home_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		if(HomePage.clickOffer(driver, log)) {
    		log.info("Inside step definitions clickedoffer tab");
    	}
	}

	@Then("^User should see Domestic flight tab$")
	public void user_should_see_Domestic_flight_tab() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		
		flag=OfferPage.validateDomesticFlightTab(driver, log);
		Assert.assertEquals(flag,true);
	}

	@Then("^close browser$")
	public void close_browser() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.close();
		driver=null;
	}

}

