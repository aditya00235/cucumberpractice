package stepDefinitions;

import java.util.Properties;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import reusableObjects.Base;
import reusableObjects.GenerateProp;
import uiStore.HomePageUI;

public class BookFlight 
{
	WebDriver driver;
	Properties prop;
	@Given("^User first opens the browsers$")
	public void user_first_opens_the_browsers() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver=Base.initializeDriver();
	}

	@Given("^Navigate to the URL Site$")
	public void navigate_to_the_URL_Site() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Properties prop=GenerateProp.generatepropobject();
        driver.get(prop.getProperty("url"));
        driver.manage().window().maximize();
	}

	@When("^user fills the from and to details with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_fills_the_from_and_to_details_with_and(String arg1, String arg2) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		WebElement element1=driver.findElement(HomePageUI.from);
		   element1.click();
		   Thread.sleep(1000L);
		   element1.sendKeys(arg1);
		   Thread.sleep(2000L);
		   element1.sendKeys(Keys.ENTER);
		   
		   WebElement element2=driver.findElement(HomePageUI.to);
		   element2.click();
		   Thread.sleep(1000L);
		   element2.sendKeys(arg2);
		   Thread.sleep(2000L);
		   element2.sendKeys(Keys.ENTER);
		   Thread.sleep(2000L);
		   
		   driver.findElement(HomePageUI.departureDate).click();
		   Thread.sleep(2000L);
		   
		   WebElement dateElement=driver.findElement(HomePageUI.date);
		   dateElement.click();
		   
		   Thread.sleep(1000L);
	}

	@When("^user click on search flight button$")
	public void user_click_on_search_flight_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(HomePageUI.searchFlightButton).click();
	    Thread.sleep(5000L);
	}

	@Then("^It shows the different flight details$")
	public void it_shows_the_different_flight_details() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    Assert.assertTrue(true);
	}

	@Then("^closes browser$")
	public void closes_browser() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    driver.close();
	    driver=null;
	}
}
